#Hemingway Child

Anpassungen des [Hemingway](http://www.andersnoren.se/teman/hemingway-wordpress-theme/) Themes für meine [Website](http://www.matthias-faller.de)

- Impressum im Footer
- Erweiterung des [Simple Social Icons](https://wordpress.org/plugins/simple-social-icons/) Plugins um weitere Icons