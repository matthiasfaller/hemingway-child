<?php
 
// Make Font Awesome available
// add_action( 'wp_enqueue_scripts', 'enqueue_font_awesome' );
// function enqueue_font_awesome() {
//     wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css' );
// }

//dequeue css from plugins
//http://speakinginbytes.com/2012/09/disable-plugins-css-file/
add_action('wp_print_styles', 'dequeue_css_from_plugins', 100);
function dequeue_css_from_plugins()  {
    wp_dequeue_style( "simple-social-icons-font" ); 
}


add_action( 'wp_enqueue_scripts', 'enqueue_fontello' );
function enqueue_fontello() {
    wp_enqueue_style( 'fontello', get_stylesheet_directory_uri() . '/fontello/css/fontello.css' );
}



add_filter( 'simple_social_default_profiles', 'custom_simple_social_default_profiles' );
function custom_simple_social_default_profiles() {
    $glyphs = array(
            'dribbble'      => '&#xe812;',
            'email'         => '&#xe802;',
            'facebook'      => '&#xe803;',
            'flickr'        => '&#xe804;',
            'github'        => '&#xe805;',
            'gplus'         => '&#xe806;',
            'instagram'     => '&#xe807;',
            'linkedin'      => '&#xe808;',
            'pinterest'     => '&#xe80a;',
            'rss'           => '&#xe80b;',
            'stumbleupon'   => '&#xe80c;',
            'tumblr'        => '&#xe80d;',
            'twitter'       => '&#xe801;',
            'vimeo'         => '&#xe80e;',
            'youtube'       => '&#xe80f;',
            'bitbucket'     => '&#xe810;',
            'pinboard'      => '&#xe813;',
            'xing'          => '&#xe809;',
            'reddit'        => '&#xe811;',
        );

    $profiles = array(
            'dribbble' => array(
                'label'   => __( 'Dribbble URI', 'ssiw' ),
                'pattern' => '<li class="social-dribbble"><a href="%s" %s>' . $glyphs['dribbble'] . '</a></li>',
            ),
            'email' => array(
                'label'   => __( 'Email URI', 'ssiw' ),
                'pattern' => '<li class="social-email"><a href="%s" %s>' . $glyphs['email'] . '</a></li>',
            ),
            'twitter' => array(
                'label'   => __( 'Twitter URI', 'ssiw' ),
                'pattern' => '<li class="social-twitter"><a href="%s" %s>' . $glyphs['twitter'] . '</a></li>',
            ),
            'facebook' => array(
                'label'   => __( 'Facebook URI', 'ssiw' ),
                'pattern' => '<li class="social-facebook"><a href="%s" %s>' . $glyphs['facebook'] . '</a></li>',
            ),
            'flickr' => array(
                'label'   => __( 'Flickr URI', 'ssiw' ),
                'pattern' => '<li class="social-flickr"><a href="%s" %s>' . $glyphs['flickr'] . '</a></li>',
            ),
            'github' => array(
                'label'   => __( 'GitHub URI', 'ssiw' ),
                'pattern' => '<li class="social-github"><a href="%s" %s>' . $glyphs['github'] . '</a></li>',
            ),
            'gplus' => array(
                'label'   => __( 'Google+ URI', 'ssiw' ),
                'pattern' => '<li class="social-gplus"><a href="%s" %s>' . $glyphs['gplus'] . '</a></li>',
            ),
            'instagram' => array(
                'label'   => __( 'Instagram URI', 'ssiw' ),
                'pattern' => '<li class="social-instagram"><a href="%s" %s>' . $glyphs['instagram'] . '</a></li>',
            ),
            'linkedin' => array(
                'label'   => __( 'Linkedin URI', 'ssiw' ),
                'pattern' => '<li class="social-linkedin"><a href="%s" %s>' . $glyphs['linkedin'] . '</a></li>',
            ),
            'pinterest' => array(
                'label'   => __( 'Pinterest URI', 'ssiw' ),
                'pattern' => '<li class="social-pinterest"><a href="%s" %s>' . $glyphs['pinterest'] . '</a></li>',
            ),
            'rss' => array(
                'label'   => __( 'RSS URI', 'ssiw' ),
                'pattern' => '<li class="social-rss"><a href="%s" %s>' . $glyphs['rss'] . '</a></li>',
            ),
            'stumbleupon' => array(
                'label'   => __( 'StumbleUpon URI', 'ssiw' ),
                'pattern' => '<li class="social-stumbleupon"><a href="%s" %s>' . $glyphs['stumbleupon'] . '</a></li>',
            ),
            'tumblr' => array(
                'label'   => __( 'Tumblr URI', 'ssiw' ),
                'pattern' => '<li class="social-tumblr"><a href="%s" %s>' . $glyphs['tumblr'] . '</a></li>',
            ),
            'vimeo' => array(
                'label'   => __( 'Vimeo URI', 'ssiw' ),
                'pattern' => '<li class="social-vimeo"><a href="%s" %s>' . $glyphs['vimeo'] . '</a></li>',
            ),
            'youtube' => array(
                'label'   => __( 'YouTube URI', 'ssiw' ),
                'pattern' => '<li class="social-youtube"><a href="%s" %s>' . $glyphs['youtube'] . '</a></li>',
            ),
            'bitbucket' => array(
                'label'   => __( 'Bitbucket URI', 'ssiw' ),
                'pattern' => '<li class="social-bitbucket"><a href="%s" %s>' . $glyphs['bitbucket'] . '</a></li>',
            ),
            'pinboard' => array(
                'label'   => __( 'Pinboard URI', 'ssiw' ),
                'pattern' => '<li class="social-pinboard"><a href="%s" %s>' . $glyphs['pinboard'] . '</a></li>',
            ),
            'xing' => array(
                'label'   => __( 'Xing URI', 'ssiw' ),
                'pattern' => '<li class="social-xing"><a href="%s" %s>' . $glyphs['xing'] . '</a></li>',
            ),
            'reddit' => array(
                'label'   => __( 'Reddit URI', 'ssiw' ),
                'pattern' => '<li class="social-reddit"><a href="%s" %s>' . $glyphs['reddit'] . '</a></li>',
            ),
        );
    return $profiles;
}